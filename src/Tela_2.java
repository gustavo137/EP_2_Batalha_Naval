import java.awt.EventQueue;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.LineBorder;
import javax.swing.*;


public class Tela_2 extends JFrame {// 5 5
    public int batman;  //variável auxiliar
    public int robin;   //variável auxiliar
    private JPanel contentPane;
    private JTable table;
    private JTextField txtDiasRestantes;
    private JTextField UltimoAtaque;
    private String linhaAtaque;
    private String colunaAtaque;
    private int diasRestantes = 50;
    private JTextField txtSinaisInimigos;
    private int sinaisInimigos = 35;
    public int base = 15;
    public int altura = 15; // altura máxima e largura 15 x 15
    
    ImageIcon myPicture = new ImageIcon("Imgs/boat.png");
    ImageIcon acerto =    new ImageIcon("Imgs/MarAcertado.png");
    ImageIcon aguaIcon =  new ImageIcon("Imgs/sprite_04.png");
    ImageIcon subIcon =   new ImageIcon("Imgs/OO-Sprites.png");


    public void criaTabela() {
    	
        table = new JTable(){  //Uso do Override para mudar o tipo de renderização padrão da Jtable
        	@Override
            public Class<?> getColumnClass(int column) {
             return ImageIcon.class;
            }
        	 

         };
    }
    
    public Tela_2() {
    	
        gradeInimiga grade;
        grade = new gradeInimiga();
        grade.marcaValores(0,0);
        
        base = grade.getBase();
        altura = grade.getAltura();
        
        String[] aux = new String[base];
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 32 * base + 150, 32 * altura + 250);///---------------------
        
        contentPane = new JPanel();
        contentPane.setBackground(Color.LIGHT_GRAY);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JButton btnVoltar = new JButton("Render-se");
        btnVoltar.setBackground(new Color(255, 255, 153));
        btnVoltar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                contentPane.setVisible (false);
            }
        });
        btnVoltar.setBounds(39, altura * 32 + 200 , 117, 25);
        contentPane.add(btnVoltar);
        
        //-------------------------------------------------tabela-----------------------------------//          
        criaTabela();
        //table.setModel(new DefaultTableModel(rows, cols));
        
        table.setBorder(new LineBorder(Color.BLACK, 2, true));  
        table.setRowHeight(32);
        //table.setrowLenght(32);
        
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                int col = table.columnAtPoint(e.getPoint());
                linhaAtaque = Integer.toString(row);
                colunaAtaque = Integer.toString(col);
                
                UltimoAtaque.setText("Ultimo Ataque :"+ linhaAtaque + colunaAtaque );
                System.out.println("posição clicada"+ row + col);
                batman = row;
                robin = col;
                
            }
        });
            
        table.setForeground(Color.WHITE);
        table.setSurrendersFocusOnKeystroke(true);
        table.setModel(new DefaultTableModel(
            new Object[][] {
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon}, 
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon}, 
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                {aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon, aguaIcon,aguaIcon, aguaIcon, aguaIcon, aguaIcon},
                

            }, aux
        ));
        
        
        table.setColumnSelectionAllowed(true);
        table.setCellSelectionEnabled(true);
        Color azulOceano = new Color(12, 159, 198);
        table.setBackground(azulOceano);
        table.setBounds(25, 25, 32*base, 32*altura); // 32 pixels multiplicados pela quantidade de linhas e colunas LARGURA/ALTURA
        contentPane.add(table);
        //----------------------------------------Botões----------------------------------//
        JLabel imgLabel = new JLabel(new ImageIcon("Imgs/ShipPanel.gif"));
        imgLabel.setBounds(32*base+40, 0, 100, 320);
        this.getContentPane().add(imgLabel);
        
        JButton btnNewButton = new JButton("Canhões");         
        btnNewButton.setBackground(new Color (255, 255, 153));
        
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                if (sinaisInimigos <= 1 || diasRestantes < 1) {
                	System.exit(0);
                    //contentPane.setVisible (false);
                }

                System.out.println(batman);
                System.out.println(robin);
                
                if (grade.marcaValores(batman,robin) == true) {
                    System.out.println("acertou");
                    table.setValueAt(subIcon, batman, robin);
                	table.updateUI();
                    sinaisInimigos = sinaisInimigos - 1;
                    txtSinaisInimigos.setText("Sinais inimigos : "+ sinaisInimigos);
                }else {
                	table.setValueAt(acerto, batman, robin);
                	table.updateUI();
                }
                String dias = Integer.toString(diasRestantes);
                diasRestantes = diasRestantes - 1;
                txtDiasRestantes.setText("Dias restantes:"+ dias );
            }
        });
        
        btnNewButton.setBounds(210, altura * 32 + 50, 117, 25);
        contentPane.add(btnNewButton);
        
        JButton btnAtkAreo = new JButton("Atk Aéreo");
        btnAtkAreo.setBackground(new Color(255, 255, 153));
        btnAtkAreo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String dias = Integer.toString(diasRestantes);
                diasRestantes = diasRestantes - 5;
                txtDiasRestantes.setText("Dias restantes:"+ dias );
            }
        });
        btnAtkAreo.setBounds(210, altura * 32 + 100, 117, 25);
        contentPane.add(btnAtkAreo);
        
        JButton btnMorteiros = new JButton("Morteiros");
        btnMorteiros.setBackground(new Color(255, 255, 153));
        btnMorteiros.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println ("morteiros ativados");
                String dias = Integer.toString(diasRestantes);
                diasRestantes = diasRestantes - 8;
                txtDiasRestantes.setText("Dias restantes:"+ dias );
            }
        });
        btnMorteiros.setBounds(210, altura * 32 + 150, 117, 25);
        contentPane.add(btnMorteiros);
        
        txtDiasRestantes = new JTextField();
        txtDiasRestantes.setBackground(new Color(255, 255, 153));
        String dias = Integer.toString(diasRestantes);
        txtDiasRestantes.setText("Dias restantes : "+ dias );
        txtDiasRestantes.setBounds(39, altura * 32 + 100, 159, 19);
        contentPane.add(txtDiasRestantes);
        txtDiasRestantes.setColumns(10);
    
        UltimoAtaque = new JTextField();
        UltimoAtaque.setBackground(new Color(255, 255, 153));
        UltimoAtaque.setText("Posição para atacar :");
        UltimoAtaque.setBounds(39, altura * 32 + 50, 159, 19);
        contentPane.add(UltimoAtaque);
        UltimoAtaque.setColumns(10);
        
        JLabel lblNewLabel = new JLabel("1 dia");
        lblNewLabel.setBounds(345, altura * 32 + 50, 70, 15);
        contentPane.add(lblNewLabel);
        
        JLabel lblNewLabel_1 = new JLabel("5 dias");
        lblNewLabel_1.setBounds(345, altura * 32 + 100, 70, 15);
        contentPane.add(lblNewLabel_1);
        
        JLabel lblNewLabel_3 = new JLabel("8 dias");
        lblNewLabel_3.setBounds(345, altura * 32 + 150, 70, 15);
        contentPane.add(lblNewLabel_3);
        
        txtSinaisInimigos = new JTextField();
        txtSinaisInimigos.setBackground(new Color(255, 255, 153));
        txtSinaisInimigos.setText("Sinais inimigos : " + sinaisInimigos);
        txtSinaisInimigos.setBounds(39, altura * 32 + 150, 159, 19);
        contentPane.add(txtSinaisInimigos);
        txtSinaisInimigos.setColumns(10);
     
    }
}


