import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
 
public class LeituraDeTexto {
	  char [] [] grade = new char[99][99];
	  int base = 0;
	  int altura = 0;
	  int linhas = 0;
	  String baseAltura = "";
	  String mat;
	  int cont = 0;
	
	void fazLeitura () {
    String nome = "Maps/map_1";  //string de leitura
 
    try {
      FileReader arq = new FileReader(nome);
      BufferedReader lerArq = new BufferedReader(arq);
      String linha = lerArq.readLine(); // lê a primeira linha
                                        // a variável "linha" recebe o valor "null" quando o processo
                                        // de repetição atingir o final do arquivo texto
      while (linha != null) { 
    	  linhas ++;
    	  if (linhas == 2) {
    		  baseAltura = linha;
    		  String[] parts = baseAltura.split(" ");
    		  String string1 = parts[0];
    		  String string2 = parts[1];
    		  base = Integer.parseInt(string1);
    		  altura = Integer.parseInt(string2);
    	  }
    	  if (linhas >= 5  && linhas < altura + 5) {           
    		  char[] chars = linha.toCharArray();
    		  for (int i = 0 ; i < chars.length ; i++) {   //10 precisa ser substituido pela largura -- RESOLVIDO
    			  grade [cont][i] = chars [i];
    		  }
    		  cont ++;
    	  }
        System.out.printf("%s\n", linha);
        linha = lerArq.readLine(); // lê da segunda até a última linha
      }
 
      arq.close();
    } catch (IOException e) {
        System.err.printf("Erro na abertura do arquivo: %s.\n",
          e.getMessage());
    }

    String[] parts = baseAltura.split(" ");
    String string1 = parts[0];
    String string2 = parts[1];
    base = Integer.parseInt(string1);
    altura = Integer.parseInt(string2);

    System.out.println("base = "+ base);      
    System.out.println("altura = "+ altura);  
    System.out.println("Mapa com " + linhas + " linhas");
    
    }
	
	char getData (int width , int height) {
		return grade [width][height];
	}
  
	int getLargura ( ) {
		return this.base;
	}
	
	int getAltura ( ) {
		return this.altura;
	}
  
}