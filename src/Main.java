import java.awt.EventQueue;

public class Main {

	public static void main(String[] args) {
		
        // • Instanciar objetos(mapa, players, inimigos, objetos de cenário)(1)
		
        gradeInimiga grade;
		grade = new gradeInimiga();
		grade.marcaValores(0,0);
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BatalhaNaval window = new BatalhaNaval();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

}
