import java.util.Arrays;

public class gradeInimiga {
	
	//public static boolean [] [] grade = new boolean[][]; // XX YY
	
	private int eixo_x ;
	private int eixo_y ; 
	
	public boolean marcaValores (int valorx, int valory) {
		
		LeituraDeTexto mapa1;
		mapa1 = new LeituraDeTexto();
		mapa1.fazLeitura();
		eixo_x = mapa1.getLargura();
		eixo_y = mapa1.getAltura();
		
		boolean [] [] grade = new boolean[eixo_x][eixo_y];
		
		  for(int altura = 0 ; altura  < eixo_y ; altura++)        //altura
	        {
	          for(int base = 0 ; base < eixo_y; base++)            //largura
	          { 
	        	if (mapa1.getData(altura,base) != '0' ) {
	        		grade[altura][base] = true;
	        	}else {
	        		grade[altura][base] = false;
	        	}
	          }
	         
	        }
		  
		  if (grade[valorx][valory] == true) {
			  grade[valorx][valory] = false;
			  return true;
		  }
		  else
			  return false;
		  
	}
	
	public int getBase () {
		return this.eixo_x;
	}
	public int getAltura () {
		return this.eixo_y;
	}
	
}
