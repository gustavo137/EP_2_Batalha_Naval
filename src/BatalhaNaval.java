import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.Color;


	
public class BatalhaNaval {
	
    JFrame frame = new JFrame();
    
	
	private JButton btnIniciarBatalha;
	public BatalhaNaval() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	
		btnIniciarBatalha = new JButton("Iniciar Batalha");
		btnIniciarBatalha.setBackground(new Color(255, 255, 153));
		btnIniciarBatalha.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		btnIniciarBatalha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tela_2 tela2 = new Tela_2();
				tela2.setVisible (true);
				frame.setVisible (false);
			}
		});
		btnIniciarBatalha.setBounds(37, 50, 174, 25);
		frame.getContentPane().add(btnIniciarBatalha);
		
		JButton btnNewButton_1 = new JButton("Recordes");
		btnNewButton_1.setBackground(new Color(255, 255, 153));
		btnNewButton_1.setBounds(37, 110, 174, 25);
		frame.getContentPane().add(btnNewButton_1);
		JLabel imgLabel = new JLabel(new ImageIcon("Imgs/boat.gif"));
        imgLabel.setBounds(0, 0, 500, 500);
        frame.getContentPane().add(imgLabel);
	}
}
